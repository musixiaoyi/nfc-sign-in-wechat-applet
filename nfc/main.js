import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()

Vue.prototype.checkLogin = function(){
	const token = uni.getStorageSync('token')
	console.log("获取到token"+token)
	if(token === ''){ // 本地没有token表示未登录
		console.log('未登录返回到登录页')
		uni.redirectTo({url:'/pages/welcome/welcome'})
		return false
	}else{
		//获取用户信息
		uni.request({
		    url: this.apiurl + '/user/user_info', //仅为示例，并非真实接口地址。
		    header: {
		        'Authorization': uni.getStorageSync('token') //自定义请求头信息
		    },
		    success: (res) => {
		        if(!res.data.data.name){
					uni.redirectTo({url:'/pages/details/details'})
					return false
				}
				
				
		    }
		});
	}
}
Vue.prototype.aginLogin = function(){
	wx.login({
		success: res => {
			uni.request({
				url: this.apiurl + '/user/login', //仅为示例，并非真实接口地址。
				data: {
					code: res.code
				},
				header: {
					'custom-header': 'hello' //自定义请求头信息
				},
				success: (res) => {
					// console.log(typeof(res.data.data) );
					this.token = res.data.data;
	
					// 存储token与用户info
					try {
						console.log(this.token, this.userInfo)
						uni.setStorageSync('token', this.token);
					} catch (e) {
						// error
						console.log(e)
					}
				}
			});
		}
	})
}

Vue.prototype.apiurl = "https://mrslu.top/onclock";